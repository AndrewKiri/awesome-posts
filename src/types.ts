export type Status = "idle" | "loading" | "loaded" | "failed";

export type MenuItem = {
  path: string;
  text: string;
  key: string;
};

export type Breadcrumb = {
  path: string;
  text: string;
};

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface Geo {
  lat: string;
  lng: string;
}

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;
  company: Company;
}

export interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface Tag {
  key: string;
  label: string;
  color?: string;
}

export interface Comment {
  postId: number;
  id: number;
  name?: string;
  email: string;
  body: string;
  tags?: Tag[];
}
