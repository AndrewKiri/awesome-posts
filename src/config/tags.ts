import { Tag } from "../types";

export const DEFAULT_TAGS: Tag[] = [
  {
    key: "react",
    label: "React",
    color: "geekblue",
  },
  {
    key: "angular",
    label: "Angular",
    color: "gold",
  },
  {
    key: "vue",
    label: "Vue",
    color: "green",
  },
  {
    key: "laravel",
    label: "Laravel",
    color: "volcano",
  },
  {
    key: "javascript",
    label: "JavaScript",
    color: "orange",
  },
  {
    key: "symfony",
    label: "Symfony",
    color: "cyan",
  },
  {
    key: "ruby-on-rails",
    label: "Ruby on Rails",
    color: "red",
  },
  {
    key: "nest-js",
    label: "NestJS",
    color: "magenta",
  },
  {
    key: "express-js",
    label: "ExpressJS",
    color: "purple",
  },
];
