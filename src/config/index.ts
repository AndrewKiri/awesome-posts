import { MenuItem } from "../types";

export const API_URL = "https://jsonplaceholder.typicode.com";

export const DEFAULT_POSTS_PER_PAGE = 10;

export const menuItems: MenuItem[] = [
  {
    path: "/",
    text: "Home",
    key: "home",
  },
  {
    path: "/counter",
    text: "Counter",
    key: "counter",
  },
  {
    path: "/posts",
    text: "Posts",
    key: "posts",
  },
];
