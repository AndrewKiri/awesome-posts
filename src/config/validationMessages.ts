/* eslint-disable */
const validationMessages = {
  required: "${name} is required!",
  types: {
    email: "${name} is not a valid email!",
  },
};

export { validationMessages };
