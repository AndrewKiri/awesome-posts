import React from "react";
import classNames from "classnames";
import { CommentOutlined } from "@ant-design/icons";
import { Badge, Typography } from "antd";
import { Link } from "react-router-dom";
import { Post as IPost, User } from "../types";

import { Loader } from "./Loader";
import { Card } from "./Card";

import styles from "./PostCard.module.scss";

const { Paragraph } = Typography;

export interface PostProps {
  post: IPost;
  user?: User;
  className?: string;
  commentsCount?: number;
  isCommentsLoading?: boolean;
  withoutActions?: boolean;
}

export function PostCard({
  post,
  user,
  commentsCount,
  isCommentsLoading,
  className,
  withoutActions = false,
}: PostProps) {
  return (
    <Card
      className={classNames(styles.card, className)}
      title={post.title}
      extra={
        <Paragraph style={{ marginBottom: 0 }}>{user?.username}</Paragraph>
      }
      actions={
        withoutActions
          ? []
          : [
              isCommentsLoading ? (
                <Loader />
              ) : (
                <Badge count={commentsCount}>
                  <CommentOutlined style={{ fontSize: "18px" }} />
                </Badge>
              ),
              <Link to={`/posts/${post.id}`}>Read more</Link>,
            ]
      }
    >
      <p>{post.body}</p>
    </Card>
  );
}
