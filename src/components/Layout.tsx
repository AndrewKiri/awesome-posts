import React, { ReactNode } from "react";

import { Layout as AntLayout } from "antd";

import styles from "./Layout.module.scss";
import { Breadcrumbs } from "./Breadcrumbs";
import { Breadcrumb } from "../types";

const { Content, Footer } = AntLayout;

export interface LayoutProps {
  children: ReactNode;
  breadcrumbs?: Breadcrumb[];
  shouldShowBreadcrumbs?: boolean;
}

export function Layout({
  children,
  breadcrumbs,
  shouldShowBreadcrumbs,
}: LayoutProps) {
  return (
    <>
      <Content className={styles.content}>
        <div className={styles.container}>
          {shouldShowBreadcrumbs && <Breadcrumbs breadcrumbs={breadcrumbs} />}
          {children}
        </div>
      </Content>
      <Footer className={styles.footer}>
        ©2021 Created by <a href="https://github.com/AndrewKiri">andrewkiri</a>
      </Footer>
    </>
  );
}
