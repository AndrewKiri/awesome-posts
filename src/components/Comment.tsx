import React, { ReactNode } from "react";
import { Comment as AntdComment } from "antd";

export interface CommentProps {
  children?: ReactNode;
  author: ReactNode;
  content: string;
  className?: string;
  actions?: ReactNode[];
}

export function Comment({
  className,
  children,
  author,
  content,
  actions,
}: CommentProps) {
  return (
    <AntdComment
      className={className}
      actions={actions}
      author={author}
      content={<p>{content}</p>}
    >
      {children}
    </AntdComment>
  );
}
