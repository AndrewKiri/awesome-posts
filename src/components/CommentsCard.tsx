import React, { Fragment, useState } from "react";
import { motion } from "framer-motion";
import { Typography, Select, Form, Input, Button, Tag as AntdTag } from "antd";

import { Card } from "./Card";
import { Comment } from "./Comment";

import styles from "./CommentsCard.module.scss";
import { validationMessages } from "../config/validationMessages";

import {
  addComment,
  removeComment,
  selectTagsByKeys,
  selectCommentsByPostId,
  selectTags,
} from "../store/features/comments/commentsSlice";
import { useStoreSelector, useStoreDispatch } from "../store/hooks";
import { Tag } from "../types";

export interface CommentsCardProps {
  postId: number;
  className?: string;
}

const { Option } = Select;
const { Title, Text } = Typography;

const list = {
  visible: {
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
  hidden: {
    transition: {
      when: "afterChildren",
    },
  },
};

const variants = {
  visible: (i: number) => ({
    opacity: 1,
    y: 0,
    transition: {
      delay: i * 0.1,
    },
  }),
  hidden: { y: -12, opacity: 0 },
};

export function CommentsCard({ postId, className }: CommentsCardProps) {
  const comentsByPostId = useStoreSelector(selectCommentsByPostId);
  const comments = comentsByPostId[postId] ?? [];
  const tags = useStoreSelector(selectTags);
  const tagsByKeys = useStoreSelector(selectTagsByKeys);
  const dispatch = useStoreDispatch();
  const [selectedTags, setSelectedTags] = useState<Tag[]>([]);

  const handleChange = (values: string[]) => {
    setSelectedTags(values.map((tagKey) => tagsByKeys[tagKey]));
  };

  const handleRemove = (id: number) => () => {
    dispatch(removeComment({ id }));
  };

  const handleSubmit = (values: { email: string; comment: string }) => {
    dispatch(addComment({ ...values, postId, selectedTags }));
  };

  return (
    <>
      <Card className={className}>
        <div className={styles.row}>
          <Title level={5}>Comments</Title>
          <Text
            className={styles.commentsCount}
          >{`(${comments?.length})`}</Text>
        </div>
        <div className={styles.commentsContainer}>
          {comments.map((comment, i) => (
            <Fragment key={comment.id}>
              <motion.div variants={list}>
                <motion.div
                  custom={i}
                  variants={variants}
                  initial="hidden"
                  animate="visible"
                >
                  <Comment
                    actions={[
                      <span
                        key="comment-remove-action"
                        onClick={handleRemove(comment.id)}
                      >
                        Remove
                      </span>,
                    ]}
                    author={
                      <div className={styles.row}>
                        <Text className={styles.author}>{comment.email}</Text>
                        {comment?.tags?.map((tag) => (
                          <Fragment key={tag.key}>
                            <AntdTag color={tag.color}>
                              {tagsByKeys[tag.key].label}
                            </AntdTag>
                          </Fragment>
                        ))}
                      </div>
                    }
                    content={comment.body}
                  />
                </motion.div>
              </motion.div>
            </Fragment>
          ))}
        </div>
      </Card>
      <Card className={className}>
        <Title level={5}>Add reply</Title>
        <Form
          name="nest-messages"
          onFinish={handleSubmit}
          validateMessages={validationMessages}
        >
          <Form.Item
            name="email"
            rules={[{ type: "email" }, { required: true }]}
          >
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item>
            <Select
              mode="multiple"
              style={{ width: "100%" }}
              placeholder="Select technologies"
              onChange={handleChange}
              optionLabelProp="label"
            >
              {tags.map((tag) => (
                <Fragment key={tag.key}>
                  <Option value={tag.key} label={tag.label}>
                    <div>{tag.label}</div>
                  </Option>
                </Fragment>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="comment" rules={[{ required: true }]}>
            <Input.TextArea placeholder="Comment" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
}
