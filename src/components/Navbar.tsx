import React, { Fragment } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import { menuItems } from "../config";

export function Navbar() {
  return (
    <Menu theme="dark" mode="horizontal">
      {menuItems.map((menuItem) => (
        <Fragment key={menuItem.path}>
          <Menu.Item key={menuItem.path}>
            <Link to={menuItem.path}>{menuItem.text}</Link>
          </Menu.Item>
        </Fragment>
      ))}
    </Menu>
  );
}
