import React, { Fragment } from "react";
import { motion } from "framer-motion";
import { Breadcrumb as AntdBreadcrumb } from "antd";
import { Link } from "react-router-dom";

import { Breadcrumb } from "../types";

import styles from "./Breadcrumbs.module.scss";

export interface BreadcrumbsProps {
  breadcrumbs?: Breadcrumb[];
}

export function Breadcrumbs({ breadcrumbs }: BreadcrumbsProps) {
  const shouldShowBreadcrumbs = breadcrumbs && breadcrumbs?.length;

  return shouldShowBreadcrumbs ? (
    <motion.div
      animate="visible"
      initial="hidden"
      variants={{
        visible: { opacity: 1, x: 0 },
        hidden: { opacity: 0, x: 24 },
      }}
      transition={{ ease: "easeOut", duration: 0.5 }}
    >
      <AntdBreadcrumb className={styles.breadcrumb}>
        {(breadcrumbs as Breadcrumb[]).map((breadcrumb) => (
          <Fragment key={breadcrumb.text}>
            <AntdBreadcrumb.Item>
              <Link to={breadcrumb.path}>{breadcrumb.text}</Link>
            </AntdBreadcrumb.Item>
          </Fragment>
        ))}
      </AntdBreadcrumb>
    </motion.div>
  ) : null;
}
