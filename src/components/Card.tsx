import React, { ReactNode } from "react";
import classNames from "classnames";
import { Card as AntdCard } from "antd";

import styles from "./PostCard.module.scss";

export interface CardProps {
  className?: string;
  children?: ReactNode;
  actions?: ReactNode[];
  extra?: ReactNode;
  title?: string;
}

export function Card({
  children,
  className,
  extra,
  actions,
  title,
}: CardProps) {
  return (
    <AntdCard
      className={classNames(styles.card, className)}
      title={title}
      extra={extra}
      actions={actions}
    >
      {children}
    </AntdCard>
  );
}
