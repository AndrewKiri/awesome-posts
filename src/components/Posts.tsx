import React, { Fragment } from "react";
import { motion } from "framer-motion";

import { Post } from "../types";

import styles from "./Posts.module.scss";

import {
  selectStatus as selectCommentsStatus,
  selectCommentsByPostId,
} from "../store/features/comments/commentsSlice";
import { selectUsersByIds } from "../store/features/users/usersSlice";
import { useStoreSelector } from "../store/hooks";

import { PostCard } from "./PostCard";
export interface PostsProps {
  posts: Post[];
}

const list = {
  visible: {
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
  hidden: {
    transition: {
      when: "afterChildren",
    },
  },
};

const variants = {
  visible: (i: number) => ({
    opacity: 1,
    y: 0,
    transition: {
      delay: i * 0.1,
    },
  }),
  hidden: { y: -12, opacity: 0 },
};

export function Posts({ posts }: PostsProps) {
  const commentsStatus = useStoreSelector(selectCommentsStatus);
  const users = useStoreSelector(selectUsersByIds);
  const commentsByPostIds = useStoreSelector(selectCommentsByPostId);

  const isCommentsLoading = commentsStatus === "loading";

  return (
    <div className={styles.container}>
      {posts.map((post, i) => (
        <Fragment key={post.id}>
          <motion.div variants={list}>
            <motion.div
              custom={i}
              variants={variants}
              initial="hidden"
              animate="visible"
            >
              <PostCard
                post={post}
                user={users.get(post.userId)}
                commentsCount={commentsByPostIds[post.id]?.length}
                isCommentsLoading={isCommentsLoading}
              />
            </motion.div>
          </motion.div>
        </Fragment>
      ))}
    </div>
  );
}
