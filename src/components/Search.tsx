import React from "react";
import { motion } from "framer-motion";
import { Input } from "antd";

import { setFilter, clearFilter } from "../store/features/posts/postsSlice";
import { useStoreDispatch } from "../store/hooks";

import styles from "./Search.module.scss";

const { Search: AntdSearch } = Input;

export function Search() {
  const dispatch = useStoreDispatch();
  return (
    <motion.div
      animate="visible"
      initial="hidden"
      variants={{
        visible: { opacity: 1, x: 0 },
        hidden: { opacity: 0, x: 24 },
      }}
      transition={{ ease: "easeOut", duration: 0.5 }}
    >
      <div className={`${styles.searchContainer} search-container`}>
        <AntdSearch
          allowClear
          placeholder="Search by username"
          onChange={({ target }) => {
            const { value } = target;

            if (!value || value === "") {
              dispatch(clearFilter());
            }
          }}
          onSearch={(value) => {
            if (value) {
              dispatch(setFilter({ value }));
            }
          }}
        />
      </div>
    </motion.div>
  );
}
