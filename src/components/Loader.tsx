import React from "react";
import { Spin } from "antd";

export interface LoaderProps {
  tip?: string;
  className?: string;
}

export function Loader({ tip, className }: LoaderProps) {
  return <Spin className={className} tip={tip} />;
}
