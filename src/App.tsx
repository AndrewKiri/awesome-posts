import React from "react";
import { Layout as AntLayout } from "antd";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import CounterPage from "./pages/counter";
import PostsPage from "./pages/posts";
import PostPage from "./pages/post";

import { Navbar } from "./components/Navbar";

import styles from "./App.module.scss";

const { Header } = AntLayout;

function App() {
  return (
    <Router>
      <AntLayout>
        <Header className={styles.header}>
          <Navbar />
        </Header>

        <Switch>
          <Route path="/counter">
            <CounterPage />
          </Route>
          <Route path="/posts" exact>
            <PostsPage />
          </Route>
          <Route path="/posts/:id">
            <PostPage />
          </Route>
          <Route path="/">
            <PostsPage />
          </Route>
        </Switch>
      </AntLayout>
    </Router>
  );
}

export default App;
