import React from "react";
import classNames from "classnames";
import { useMount } from "react-use";
import { Pagination } from "antd";

import { Layout } from "../../components/Layout";
import { Posts } from "../../components/Posts";
import { Loader } from "../../components/Loader";
import { Search } from "../../components/Search";

import styles from "./Posts.module.scss";

import {
  fetchComments,
  selectStatus as selectCommentsStatus,
} from "../../store/features/comments/commentsSlice";
import {
  fetchUsers,
  selectStatus as selectUsersStatus,
} from "../../store/features/users/usersSlice";
import {
  fetchPosts,
  selectPagination,
  selectCurrentPagePosts,
  selectStatus as selectPostsStatus,
  setPage,
} from "../../store/features/posts/postsSlice";
import { useStoreDispatch, useStoreSelector } from "../../store/hooks";

const breadcrumbs = [
  {
    path: "/posts",
    text: "Posts",
  },
];

export default function PostsPage() {
  const { currentPagePosts, total } = useStoreSelector(selectCurrentPagePosts);

  const postsStatus = useStoreSelector(selectPostsStatus);
  const commentsStatus = useStoreSelector(selectCommentsStatus);
  const usersStatus = useStoreSelector(selectUsersStatus);

  const { currentPage, pageSize } = useStoreSelector(selectPagination);
  const dispatch = useStoreDispatch();

  const isLoading = postsStatus === "loading";
  const isEverythingLoaded = [postsStatus, commentsStatus, usersStatus].every(
    (i) => i === "loaded"
  );

  useMount(() => {
    if (!isEverythingLoaded) {
      dispatch(fetchPosts());
      dispatch(fetchUsers());
      dispatch(fetchComments());
    }
  });

  return (
    <Layout breadcrumbs={breadcrumbs} shouldShowBreadcrumbs={!isLoading}>
      {isLoading ? (
        <div className={styles.content}>
          <Loader className={styles.loader} tip="Loading..." />
        </div>
      ) : (
        <div className={classNames("posts-page", styles.content)}>
          <Search />
          <Posts posts={currentPagePosts} />
          <Pagination
            showSizeChanger
            current={currentPage}
            total={total}
            pageSize={pageSize}
            onChange={(page, pageSize) => dispatch(setPage({ page, pageSize }))}
          />
        </div>
      )}
    </Layout>
  );
}
