import React, { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useParams } from "react-router-dom";

import { Layout } from "../../components/Layout";
import { useStoreSelector } from "../../store/hooks";

import { PostCard } from "../../components/PostCard";
import { CommentsCard } from "../../components/CommentsCard";

import { selectPosts } from "../../store/features/posts/postsSlice";
import { Breadcrumb } from "../../types";

import styles from "./Post.module.scss";

const list = {
  visible: {
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
  hidden: {
    transition: {
      when: "afterChildren",
    },
  },
};

const variants = {
  visible: (i: number) => ({
    opacity: 1,
    x: 0,
    transition: {
      delay: i * 0.1,
    },
  }),
  hidden: { x: 12, opacity: 0 },
};

export default function PostsPage() {
  const [breadcrumbs, setBreadcrumbs] = useState<Breadcrumb[]>([]);
  const { id } = useParams<{ id: string }>();

  const posts = useStoreSelector(selectPosts);

  const post = posts.find((item) => `${item.id}` === id);

  useEffect(() => {
    if (post) {
      setBreadcrumbs([
        { path: "/posts", text: "Posts" },
        { path: `/posts/${id}`, text: post.title },
      ]);
    }
  }, [id, post, setBreadcrumbs]);

  return (
    <Layout breadcrumbs={breadcrumbs} shouldShowBreadcrumbs>
      <div className={styles.content}>
        <motion.div variants={list}>
          {post && (
            <motion.div
              custom={1}
              variants={variants}
              initial="hidden"
              animate="visible"
            >
              <PostCard className={styles.card} post={post} withoutActions />
            </motion.div>
          )}
          {post && (
            <motion.div
              custom={1}
              variants={variants}
              initial="hidden"
              animate="visible"
            >
              <CommentsCard className={styles.card} postId={post.id} />
            </motion.div>
          )}
        </motion.div>
      </div>
    </Layout>
  );
}
