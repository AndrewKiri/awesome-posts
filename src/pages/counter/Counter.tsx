import React from "react";

import { Layout } from "../../components/Layout";
import { Counter } from "../../components/Counter";

export default function CounterPage() {
  return (
    <Layout>
      <Counter />
    </Layout>
  );
}
