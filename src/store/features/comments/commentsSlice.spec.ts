import commentsReducer, {
  CommentsState,
  addComment,
  removeComment,
} from "./commentsSlice";
import { DEFAULT_TAGS } from "../../../config/tags";

describe("comments reducer", () => {
  const defaultComment = {
    postId: 500100,
    id: 50,
    email: "andrew.y@mail.com",
    body: "this is awesome",
  };
  const initialState: CommentsState = {
    data: [defaultComment],
    commentsByPostId: {
      [defaultComment.postId]: [defaultComment],
    },
    status: "idle",
    tags: DEFAULT_TAGS,
  };

  it("should handle adding comment", () => {
    const newComment = {
      postId: 100500,
      email: "andrew.k@mail.com",
      comment: "this is great",
    };
    const actual = commentsReducer(initialState, addComment(newComment));

    expect(actual.data[actual.data.length - 1].email).toEqual(newComment.email);
    expect(
      actual.commentsByPostId[newComment.postId]?.find(
        (i) => i.email === newComment.email
      )?.email
    ).toEqual(newComment.email);
  });

  it("should handle removing comment", () => {
    const actual = commentsReducer(
      initialState,
      removeComment({ id: defaultComment.id })
    );

    expect(actual.data.findIndex((i) => i.id === defaultComment.id)).toEqual(
      -1
    );
    expect(actual.commentsByPostId[defaultComment.postId]).toEqual(undefined);
  });
});
