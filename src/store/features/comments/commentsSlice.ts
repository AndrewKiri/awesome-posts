import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetch } from "./commentsAPI";
import { Comment, Status, Tag } from "../../../types";
import { RootState } from "../..";
import { DEFAULT_TAGS } from "../../../config/tags";

export type CommentsByPostIdRecord = Record<number, Comment[]>;
export type CommentCountByPostIdRecord = Record<number, number>;

export interface CommentsState {
  data: Comment[];
  commentsByPostId: CommentsByPostIdRecord;
  status: Status;
  tags: Tag[];
}

const composeCommentsByPostId = (
  comments: Comment[]
): CommentsByPostIdRecord => {
  const commentsByPostId: CommentsByPostIdRecord = {};

  comments.forEach((comment) => {
    commentsByPostId[comment.postId] = [
      ...(commentsByPostId[comment.postId] ?? []),
      comment,
    ];
  });

  return commentsByPostId;
};

const initialState: CommentsState = {
  data: [],
  commentsByPostId: {},
  status: "idle",
  tags: DEFAULT_TAGS,
};

export const fetchComments = createAsyncThunk("comments/fetch", async () => {
  const response = await fetch();
  return response.data;
});

export const commentsSlice = createSlice({
  name: "comments",
  initialState,
  reducers: {
    addComment: (state, action) => {
      const { postId, email, comment, selectedTags } = action.payload;

      if (email && comment) {
        const item = {
          email,
          body: comment,
          id: Math.floor(Math.random() * 1000),
          postId,
          tags: selectedTags,
        };

        state.data.push(item);

        if (!state.commentsByPostId[postId])
          state.commentsByPostId[postId] = [];

        state.commentsByPostId[postId].push(item);
      }
    },
    removeComment: (state, action) => {
      const { id } = action.payload;
      const postIndex = state.data.findIndex((i) => i.id === id);
      const isPostExist = postIndex > -1;

      if (isPostExist) {
        const comments = state.data.filter((i) => i.id !== id);

        state.data = comments;
        state.commentsByPostId = composeCommentsByPostId(comments);
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchComments.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchComments.fulfilled, (state, action) => {
        state.status = "loaded";
        state.data = action.payload;

        state.commentsByPostId = composeCommentsByPostId(state.data);
      });
  },
});

export const { addComment, removeComment } = commentsSlice.actions;

export const selectStatus = (state: RootState) => state.comments.status;
export const selectComments = (state: RootState) => state.comments.data;
export const selectCommentsByPostId = (state: RootState) =>
  state.comments.commentsByPostId;
export const selectTags = (state: RootState) => state.comments.tags;
export const selectTagsByKeys = (state: RootState) =>
  Object.fromEntries(state.comments.tags.map((tag) => [tag.key, tag]));

export default commentsSlice.reducer;
