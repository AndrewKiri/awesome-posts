import axios, { AxiosResponse } from "axios";
import { API_URL } from "../../../config";

export function fetch(): Promise<AxiosResponse> {
  return axios.get(`${API_URL}/comments`);
}
