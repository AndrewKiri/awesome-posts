import postsReducer, {
  PostsState,
  setFilter,
  clearFilter,
  setPage,
} from "./postsSlice";
import { DEFAULT_POSTS_PER_PAGE } from "../../../config";

describe("posts reducer", () => {
  const initialState: PostsState = {
    data: [],
    currentPage: 1,
    pageSize: DEFAULT_POSTS_PER_PAGE,
    filter: "",
    status: "idle",
  };

  it("should set filter by post author properly", () => {
    const newFilter = {
      value: "andrew.k@mail.com",
    };
    const actual = postsReducer(initialState, setFilter(newFilter));

    expect(actual.filter).toEqual(newFilter.value);
  });

  it("should clear filter correctly", async () => {
    const actual = postsReducer(initialState, clearFilter());

    expect(actual.filter).toEqual("");
  });

  it("should set pagination values correctly", async () => {
    const newPaginationValues = { page: 2, pageSize: 25 };
    const actual = postsReducer(initialState, setPage(newPaginationValues));

    expect(actual.currentPage).toEqual(newPaginationValues.page);
    expect(actual.pageSize).toEqual(newPaginationValues.pageSize);
  });
});
