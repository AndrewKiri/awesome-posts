import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetch } from "./postsAPI";
import { Post, Status } from "../../../types";
import { RootState } from "../..";

import { DEFAULT_POSTS_PER_PAGE } from "../../../config";

export interface PostsState {
  data: Post[];
  currentPage: number;
  pageSize: number;
  filter: string;
  status: Status;
}

const initialState: PostsState = {
  data: [],
  currentPage: 1,
  pageSize: DEFAULT_POSTS_PER_PAGE,
  filter: "",
  status: "idle",
};

export const fetchPosts = createAsyncThunk("posts/fetch", async () => {
  const response = await fetch();
  return response.data;
});

export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setFilter: (state, action) => {
      const { value } = action.payload;

      state.filter = value;
      state.currentPage = 1;
    },
    clearFilter: (state) => {
      state.filter = "";
    },
    setPage: (state, action) => {
      const { page, pageSize } = action.payload;

      state.currentPage = page;
      state.pageSize = pageSize;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = "loaded";
        state.data = action.payload;
      });
  },
});

export const { setPage, setFilter, clearFilter } = postsSlice.actions;

export const selectStatus = (state: RootState) => state.posts.status;
export const selectPosts = (state: RootState) => state.posts.data;
export const selectCurrentPagePosts = (state: RootState) => {
  const { usersByUsername: users } = state.users;
  const { data, currentPage, pageSize, filter } = state.posts;

  const isFilterActive = filter && filter?.length > 0;
  const foundUser = users.get(filter);
  const posts =
    isFilterActive && foundUser
      ? data.filter((i) => i.userId === foundUser?.id)
      : data;
  const total = posts.length;

  return {
    currentPagePosts: posts.slice(
      (currentPage - 1) * pageSize,
      currentPage * pageSize
    ),
    total,
  };
};
export const selectPagination = (state: RootState) => ({
  currentPage: state.posts.currentPage,
  pageSize: state.posts.pageSize,
});

export default postsSlice.reducer;
