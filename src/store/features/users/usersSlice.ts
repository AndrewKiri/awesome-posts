import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetch } from "./usersAPI";
import { User, Status } from "../../../types";
import { RootState } from "../..";

export interface UsersState {
  data: User[];
  usersByUsername: Map<string, User>;
  usersByIds: Map<number, User>;
  status: Status;
}

const initialState: UsersState = {
  data: [],
  usersByUsername: new Map(),
  usersByIds: new Map(),
  status: "idle",
};

export const fetchUsers = createAsyncThunk("users/fetch", async () => {
  const response = await fetch();
  return response.data;
});

export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.status = "loaded";
        state.data = action.payload;
        state.usersByUsername = new Map(
          (action.payload as User[]).map((i) => [i.username, i])
        );
        state.usersByIds = new Map(
          (action.payload as User[]).map((i) => [i.id, i])
        );
      });
  },
});

export const selectStatus = (state: RootState) => state.users.status;
export const selectUsers = (state: RootState) => state.users.data;
export const selectUsersByUsername = (state: RootState) =>
  state.users.usersByUsername;
export const selectUsersByIds = (state: RootState) => state.users.usersByIds;

export default usersSlice.reducer;
