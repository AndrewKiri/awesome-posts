# Note from author

## Technologies used
- `@reduxjs/toolkit` as state management solution
- `antd` as component library of choice (well written fully on TS)
- `classnames` for easier management of conditional classnames
- `react-use` for utility hooks
- Typescript 🖤 (the only scalable & solid way to write JS)
- `axios` for async requests to API
- `framer-motion` for sleek animations 🔥

## Cool features
- App is automatically deployed using Vercel, [see it live](https://awesome-posts.vercel.app/)
- App uses `react-router-dom` and has `Breadcrumbs` showing the path where the user currently is
- App loads posts from API only once on mount
- User can search by author name
- User can paginate back and forth
- User can change the number of posts shown per page
- Search by author name resets pagination (as logically expected)
- Removing string value from search input resets the search (as logically expected)
- App is mobile-ready (via CSS media queries)
- Post card shows the number of comments left to the post (on Posts page)
- Post page shows the post itself & comments
- User can leave a comment to a post
- User can remove a comment from a post
- User can pick tags via Select component that will be shown in the comment once it's added to the list (submitted)
- Comments form has validation, so no empty submits 🤓
- Jumping between Posts & Post pages are without page refresh (thanks to `react-router-dom`)
- App has some basic tests written for reducers

# Development timeline
June 17th, (5 hours):
```
- Initialize project
- Add basic skeleton & routes
- Add prettier
- Add antd
- Add react-use
- Add Layout for pages
- Add posts page
- Add posts redux slice
```
  
June 20th, (6 hours):
```
- Separate menu into Navbar component
- Posts page adjustments (pagination & postsSlice)
- Icon indicator for comments
- Add users slice
- Add support for username searching
- Allow Loader to accept tip prop
- Add Post page
- Create comments redux slice with fetching
- Wire up comment counts on Posts page
- reorder components/Posts to be small minimalistic component
- move page logics to pages/Posts
- Move out search elements & logic to its own component
- Breadcrumbs for post page & layout fixes
```
  
June 21st, (9 hours):
```
- Mobile & tablet styles adjustments
- Add classnames package for better managing incoming className props
- Add Post page basic skeleton
- Wire Post page components to redux store
- Create reusable Card component
- Use common Card in PostCard & CommentCard
- Basic comment form
- Prevent data from overfetching when visiting Posts page again
- Add comments counter to Post page
```
  
June 22nd, (10 hours):
```
- Allow leaving off comments
- Add support for adding tags to comment
- Adjust commentsSlice to allow tags to be attached to comment
- Style adjustments
- Allow removal of comments
- Add animations to various components
- Fix bug with search by user & pagination
- Adjust tests
- Fix breadcrumb conditional showing logic
- Project markdown presentation
```

# Development instructions

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
